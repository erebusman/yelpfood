import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Search from './src/screens/Search';
import ResultShow from './src/screens/ResultShow';

const navigator = createStackNavigator(
  {
    Search: Search,
    ResultShow: ResultShow,
  },
  {
    initialRouteName: 'Search',
    defaultNavigationOptions: {
      title: 'Restaurant Search',
    },
  }
);

export default createAppContainer(navigator);
