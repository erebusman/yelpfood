import { useEffect, useState } from 'react';
import yelp from '../api/yelp';

export default () => {
  const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  const searchApi = async (searchTerm) => {
    try {
      const response = await yelp.get('/search', {
        params: {
          limit: 10,
          term: searchTerm,
          location: 'sacramento',
        },
      });
      setResults(response.data.businesses);
    } catch (err) {
      setErrorMessage('Something went wrong, please try again later');
    }
  };

  //call searchApi when component is first rendered
  useEffect(() => {
    searchApi('pizza');
  }, []);

  return [searchApi, results, errorMessage];
};
