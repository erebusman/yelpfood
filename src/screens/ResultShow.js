import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';
import yelp from '../api/yelp';

const ResultShow = ({ navigation }) => {
  const [result, setResult] = useState(null);

  const id = navigation.getParam('id');
  console.log(result);
  const getResult = async (id) => {
    const response = await yelp.get(`/${id}`);
    setResult(response.data);
  };

  useEffect(() => {
    getResult(id);
  }, []);

  if (!result) {
    return null;
  }

  return (
    <View>
      <Text>{result.name}</Text>
      <Text>Phone: {result.phone}</Text>
      <Text>Rating: {result.rating}</Text>
      <Text>Rating: {result.review_count}</Text>
      <FlatList
        data={result.photos}
        keyExtractor={(photo) => photo}
        renderItem={({ item }) => {
          return <Image source={{ uri: item }} style={style.image}></Image>;
        }}
      ></FlatList>
    </View>
  );
};

const style = StyleSheet.create({
  image: {
    height: 200,
    width: 300,
  },
});

export default ResultShow;
